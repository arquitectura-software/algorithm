package mx.itesm.finalarquitectura.algorithm.service;

import com.google.gson.Gson;
import mx.itesm.finalarquitectura.algorithm.domain.Town;
import mx.itesm.finalarquitectura.algorithm.pojo.Message;
import mx.itesm.finalarquitectura.algorithm.pojo.MessageResult;
import mx.itesm.finalarquitectura.algorithm.pojo.customresponses.DistanceMatrixResult;
import mx.itesm.finalarquitectura.algorithm.resources.TownsSingleton;
import okhttp3.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.BufferedReader;
import java.io.Reader;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

@Service
public class DistanceService {

    private static final Logger logger = LoggerFactory.getLogger(DistanceService.class);

    private OkHttpClient client;

    @Value("${distancematrix.url}")
    private String distanceUrl;

    @PostConstruct
    private void initService() {
        this.client = new OkHttpClient.Builder()
                .connectTimeout(20, TimeUnit.SECONDS)
                .writeTimeout(20, TimeUnit.SECONDS)
                .readTimeout(20, TimeUnit.SECONDS)
                .build();
    }

    public MessageResult initDistances() {
        int success = 0;
        int failed = 0;

        double distances[][] = new double[111][111];

        for(int i = 0; i < distances.length; i++) {
            for(int j = 0; j < distances[i].length; j++) {
                distances[i][j] = -1.0;
            }
        }

        for(int i = 0; i < 111; i++) {
            Town origin = TownsSingleton.getInstance().getTown(i+1).get();

            LinkedList<Town> destinations = new LinkedList<>();
            Optional<DistanceMatrixResult> distanceMatrixResult;
            int j = 0;
            int counter = 0;

            // Get 0 - 19
            while(counter < j + 20) {
                destinations.add(TownsSingleton.getInstance().getTown(counter+1).get());
                counter++;
            }
            distanceMatrixResult = getDistancesByOrigin(origin, destinations.toArray(new Town[destinations.size()]));
            if(distanceMatrixResult.isPresent()) {
                for(int k = 0; k < distanceMatrixResult.get().rows[0].elements.length; k++) {
                    if(distanceMatrixResult.get().rows[0].elements[k].status.equals("OK")) {
                        distances[i][j + k] = distanceMatrixResult.get().rows[0].elements[k].distance.value;
                        success++;
                    } else {
                        distances[i][j + k] = -1;
                        failed++;
                    }
                }
            } else {
                failed += 20;
            }

            // Get 20 - 39
            destinations = new LinkedList<>();
            j = 20;
            counter = 20;
            while(counter < j + 20) {
                destinations.add(TownsSingleton.getInstance().getTown(counter+1).get());
                counter++;
            }
            distanceMatrixResult = getDistancesByOrigin(origin, destinations.toArray(new Town[destinations.size()]));
            if(distanceMatrixResult.isPresent()) {
                for(int k = 0; k < distanceMatrixResult.get().rows[0].elements.length; k++) {
                    if(distanceMatrixResult.get().rows[0].elements[k].status.equals("OK")) {
                        distances[i][j + k] = distanceMatrixResult.get().rows[0].elements[k].distance.value;
                        success++;
                    } else {
                        distances[i][j + k] = -1;
                        failed++;
                    }
                }
            } else {
                failed += 20;
            }

            // Get 40 - 59
            destinations = new LinkedList<>();
            j = 40;
            counter = 40;
            while(counter < j + 20) {
                destinations.add(TownsSingleton.getInstance().getTown(counter+1).get());
                counter++;
            }
            distanceMatrixResult = getDistancesByOrigin(origin, destinations.toArray(new Town[destinations.size()]));
            if(distanceMatrixResult.isPresent()) {
                for(int k = 0; k < distanceMatrixResult.get().rows[0].elements.length; k++) {
                    if(distanceMatrixResult.get().rows[0].elements[k].status.equals("OK")) {
                        distances[i][j + k] = distanceMatrixResult.get().rows[0].elements[k].distance.value;
                        success++;
                    } else {
                        distances[i][j + k] = -1;
                        failed++;
                    }
                }
            } else {
                failed += 20;
            }

            // Get 60 - 79
            destinations = new LinkedList<>();
            j = 60;
            counter = 60;
            while(counter < j + 20) {
                destinations.add(TownsSingleton.getInstance().getTown(counter+1).get());
                counter++;
            }
            distanceMatrixResult = getDistancesByOrigin(origin, destinations.toArray(new Town[destinations.size()]));
            if(distanceMatrixResult.isPresent()) {
                for(int k = 0; k < distanceMatrixResult.get().rows[0].elements.length; k++) {
                    if(distanceMatrixResult.get().rows[0].elements[k].status.equals("OK")) {
                        distances[i][j + k] = distanceMatrixResult.get().rows[0].elements[k].distance.value;
                        success++;
                    } else {
                        distances[i][j + k] = -1;
                        failed++;
                    }
                }
            } else {
                failed += 20;
            }

            // Get 80 - 99
            destinations = new LinkedList<>();
            j = 80;
            counter = 80;
            while(counter < j + 20) {
                destinations.add(TownsSingleton.getInstance().getTown(counter+1).get());
                counter++;
            }
            distanceMatrixResult = getDistancesByOrigin(origin, destinations.toArray(new Town[destinations.size()]));
            if(distanceMatrixResult.isPresent()) {
                for(int k = 0; k < distanceMatrixResult.get().rows[0].elements.length; k++) {
                    if(distanceMatrixResult.get().rows[0].elements[k].status.equals("OK")) {
                        distances[i][j + k] = distanceMatrixResult.get().rows[0].elements[k].distance.value;
                        success++;
                    } else {
                        distances[i][j + k] = -1;
                        failed++;
                    }
                }
            } else {
                failed += 20;
            }

            // Get 100 - 110
            destinations = new LinkedList<>();
            j = 100;
            counter = 100;
            while(counter < j + 11) {
                destinations.add(TownsSingleton.getInstance().getTown(counter+1).get());
                counter++;
            }
            distanceMatrixResult = getDistancesByOrigin(origin, destinations.toArray(new Town[destinations.size()]));
            if(distanceMatrixResult.isPresent()) {
                for(int k = 0; k < distanceMatrixResult.get().rows[0].elements.length; k++) {
                    if(distanceMatrixResult.get().rows[0].elements[k].status.equals("OK")) {
                        distances[i][j + k] = distanceMatrixResult.get().rows[0].elements[k].distance.value;
                        success++;
                    } else {
                        distances[i][j + k] = -1;
                        failed++;
                    }
                }
            } else {
                failed += 11;
            }

            logger.info(String.format("Finish row: %d, total errors: %d", i, failed));
        }

        StringBuilder stringBuilder = new StringBuilder();
        for(int i = 0; i < distances.length; i++) {
            for(int j = 0; j < distances[i].length; j++) {
                if(j==0) {
                    stringBuilder.append(String.format("%f", distances[i][j]));
                } else {
                    stringBuilder.append(String.format(",%f", distances[i][j]));
                }
            }
            if(i < distances.length - 1) {
                stringBuilder.append(",\n");
            }
        }
        System.out.println(stringBuilder.toString());

        // Write result string to csv file
        try {
            List<String> lines = Arrays.asList(stringBuilder.toString(), "");
            Path file = Paths.get("distances_res.csv");
            Files.write(file, lines, Charset.forName("UTF-8"));
        } catch (Exception e) {
            logger.error(e.getMessage());
        }

        MessageResult result = new MessageResult();
        if(failed > 0) {
            result.setErrorCode(500);
            result.setMessage(new Message(String.format("Failed %d: %s", failed, stringBuilder.toString())));
        } else {
            result.setMessage(new Message(String.format("Success %d: %s", success, stringBuilder.toString())));
        }
        return result;
    }

    public Optional<DistanceMatrixResult> getDistancesByOrigin(Town origin, Town[] destinations) {
        String url = this.distanceUrl;
        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        Response response = null;
        try {
            logger.info("Requesting url: " + url);

            String json = new Gson().toJson(new RequestBodyJSON(origin, destinations));
            RequestBody body = RequestBody.create(JSON, json);
            Request request = new Request.Builder()
                    .url(url)
                    .post(body)
                    .build();
            response = client.newCall(request).execute();

            if(response.code() >= 200 && response.code() < 300) {
                Reader in = response.body().charStream();
                BufferedReader reader = new BufferedReader(in);

                DistanceMatrixResult results = new Gson().fromJson(reader, DistanceMatrixResult.class);
                reader.close();
                if(results == null) {
                    throw new Exception("Error parsing JSON: DistanceMatrixRequest");
                }
                if(!results.status.equals("OK") || results.rows.length < 1 || results.rows[0].elements.length != destinations.length) {
                    throw new Exception(results.error_message + ": " + results.status + ", url: " + url);
                }

                return Optional.of(results);
            } else {
                throw new Exception("Wrong status code: DistanceMatrixRequest");
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }

        if(response != null && response.body() != null) {
            response.body().close();
        }

        return Optional.empty();
    }

    private class RequestBodyJSON {
        public Town origin;
        public Town[] destinations;

        RequestBodyJSON(Town origin, Town[] destinations){
            this.origin = origin;
            this.destinations = destinations;
        }
    }
}
