package mx.itesm.finalarquitectura.algorithm.service;

import mx.itesm.finalarquitectura.algorithm.domain.ShortestRoute;
import mx.itesm.finalarquitectura.algorithm.domain.Town;
import mx.itesm.finalarquitectura.algorithm.pojo.Result;
import mx.itesm.finalarquitectura.algorithm.pojo.customrequests.ShortestRouteRequest;
import mx.itesm.finalarquitectura.algorithm.resources.DistancesSingleton;
import mx.itesm.finalarquitectura.algorithm.resources.TownsSingleton;
import mx.itesm.finalarquitectura.algorithm.validation.ShortestRouteValidation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.Random;

@Service
public class ShortestRouteService {

    private static final Logger logger = LoggerFactory.getLogger(ShortestRouteService.class);


    @Autowired
    private ShortestRouteAlgorithmService shortestRouteAlgorithmService;

    // Obtains the shortest route from an origin town to a destination town passing
    // through every town in the process
    public Result<ShortestRoute> getShortestRoute(ShortestRouteRequest shortestRouteRequest) {
        Result<ShortestRoute> result = ShortestRouteValidation.validate(shortestRouteRequest);

        if(result.getErrorCode() != null && result.getErrorCode() > 0) {
            return result;
        }

        Town startTown = TownsSingleton.getInstance().getTown(shortestRouteRequest.getTown1().getId()).get();
        Town finishTown = TownsSingleton.getInstance().getTown(shortestRouteRequest.getTown2().getId()).get();

        ShortestRoute shortestRoute = new ShortestRoute();
        shortestRoute.setOrigin(startTown);
        shortestRoute.setDestination(finishTown);
        shortestRoute.setTowns(shortestRouteAlgorithmService.getShortestRoute(shortestRoute.getOrigin(), shortestRoute.getDestination()));

        // Get total distance
        shortestRoute.setDistance(shortestRouteAlgorithmService.calculateRouteDistance(shortestRoute.getTowns()));

        // Get total time
        Random rand = new Random();
        double velocity = rand.nextInt((120 - 80) + 1) + 80;
        shortestRoute.setTime((shortestRoute.getDistance() / 1000.0) / velocity);

        // Get total gasoline (liters)
        shortestRoute.setGasLiters((shortestRoute.getDistance() / 1000.0) / shortestRouteRequest.getKml());

        // Get distances between towns
        shortestRoute.setDistancesToTowns(this.getDistancesInRoute(shortestRoute.getTowns()));

        result.setData(Optional.of(shortestRoute));
        return result;
    }

    private Double[] getDistancesInRoute(Town[] route) {
        Double[] distances = new Double[route.length - 1];

        for(int i = 0; i < distances.length; i++) {
            Optional<Double> currentDistance = DistancesSingleton.getInstance().getDistance(route[i].getId(), route[i+1].getId());
            distances[i] = currentDistance.isPresent() ? currentDistance.get() : 0.0;
        }

        return distances;
    }

}
