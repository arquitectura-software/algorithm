package mx.itesm.finalarquitectura.algorithm.validation;


import mx.itesm.finalarquitectura.algorithm.domain.ShortestRoute;
import mx.itesm.finalarquitectura.algorithm.pojo.Message;
import mx.itesm.finalarquitectura.algorithm.pojo.Result;
import mx.itesm.finalarquitectura.algorithm.pojo.customrequests.ShortestRouteRequest;

// Validates a request to obtain the shortest route from one town to another passing through all the other
// 109 towns first
public class ShortestRouteValidation {

    public static Result<ShortestRoute> validate(ShortestRouteRequest shortestRouteRequest) {
        Result<ShortestRoute> result = new Result<>();
        result.setErrorCode(null);

        String message = "";

        if(shortestRouteRequest == null) {
            result.setErrorCode(400);
            message += "El cuerpo del post no puede ser nulo. ";
        } else {
            if(shortestRouteRequest.getTown1() == null) {
                result.setErrorCode(400);
                message += "Se tiene que enviar el pueblo inicial. ";
            } else {
                if(shortestRouteRequest.getTown1().getId() == null || shortestRouteRequest.getTown1().getId() < 1 || shortestRouteRequest.getTown1().getId() > 111) {
                    result.setErrorCode(400);
                    message += "Id no válido para el pueblo inicial. ";
                }
            }

            if(shortestRouteRequest.getTown2() == null) {
                result.setErrorCode(400);
                message += "Se tiene que enviar el pueblo final. ";
            } else {
                if(shortestRouteRequest.getTown2().getId() == null || shortestRouteRequest.getTown2().getId() < 1 || shortestRouteRequest.getTown2().getId() > 111) {
                    result.setErrorCode(400);
                    message += "Id no válido para el pueblo final. ";
                }
            }

            if(shortestRouteRequest.getKml() == null || shortestRouteRequest.getKml() <= 0.0) {
                result.setErrorCode(400);
                message += "Se tiene que enviar el consumo de combustible. ";
            }
        }

        result.setMessage(new Message(message));

        return result;
    }
}
