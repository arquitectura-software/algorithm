package mx.itesm.finalarquitectura.algorithm.pojo.customresponses;

public class DistanceMatrixElement {
    public DistanceMatrixElementObject distance;
    public DistanceMatrixElementObject duration;
    public String status;
}
