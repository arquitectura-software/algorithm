package mx.itesm.finalarquitectura.algorithm.pojo.customresponses;

public class DistanceMatrixResult {
    public DistanceMatrixRow[] rows;
    public String status;
    public String error_message;
}
