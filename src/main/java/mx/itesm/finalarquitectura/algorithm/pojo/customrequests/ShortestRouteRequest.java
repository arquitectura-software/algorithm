package mx.itesm.finalarquitectura.algorithm.pojo.customrequests;

import mx.itesm.finalarquitectura.algorithm.domain.Town;

public class ShortestRouteRequest {

    private Town town1;
    private Town town2;
    private Double kml;

    public Town getTown1() {
        return town1;
    }

    public void setTown1(Town town1) {
        this.town1 = town1;
    }

    public Town getTown2() {
        return town2;
    }

    public void setTown2(Town town2) {
        this.town2 = town2;
    }

    public Double getKml() {
        return kml;
    }

    public void setKml(Double kml) {
        this.kml = kml;
    }

    @Override
    public String toString() {
        return "ShortestRouteRequest{" +
                "town1=" + town1 +
                ", town2=" + town2 +
                ", kml=" + kml +
                '}';
    }
}
