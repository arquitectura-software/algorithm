package mx.itesm.finalarquitectura.algorithm.endpoint;

import mx.itesm.finalarquitectura.algorithm.domain.ShortestRoute;
import mx.itesm.finalarquitectura.algorithm.domain.Town;
import mx.itesm.finalarquitectura.algorithm.pojo.Result;
import mx.itesm.finalarquitectura.algorithm.pojo.customrequests.ShortestRouteRequest;
import mx.itesm.finalarquitectura.algorithm.service.ShortestRouteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Component
@Path("")
@Produces(MediaType.APPLICATION_JSON)
public class ShortestRouteEndpoint {

    @Autowired
    private ShortestRouteService shortestRouteService;

    @GET
    @Path("/shortest_route")
    public Response getShortestRoute(@QueryParam("town1") String town1, @QueryParam("town2") String town2, @QueryParam("kml") String kml ){
        ShortestRouteRequest request = new ShortestRouteRequest();
        request.setTown1(new Town());
        request.setTown2(new Town());
        if(town1 != null) {
            try {
                Integer town1Id = Integer.parseInt(town1.trim());
                request.getTown1().setId(town1Id);
            } catch (Exception e) {
                request.getTown1().setId(0);
            }
        } else {
            request.getTown1().setId(null);
        }
        if(town2 != null) {
            try {
                Integer town2Id = Integer.parseInt(town2.trim());
                request.getTown2().setId(town2Id);
            } catch (Exception e) {
                request.getTown2().setId(0);
            }
        } else {
            request.getTown2().setId(null);
        }
        if(kml != null) {
            try {
                Double kmlDouble = Double.parseDouble(kml.trim());
                request.setKml(kmlDouble);
            } catch (Exception e) {
                request.setKml(null);
            }
        } else {
            request.setKml(null);
        }

        Result<ShortestRoute> result = shortestRouteService.getShortestRoute(request);
        Response response;
        if(result.getData().isPresent()) {
            response = Response.ok(result.getData().get()).build();
        } else {
            if(result.getErrorCode() != null && result.getErrorCode() > 0) {
                response = Response.status(result.getErrorCode()).entity(result.getMessage()).build();
            } else {
                response = Response.status(500).entity("Error de servidor. ").build();
            }
        }
        return response;
    }
}
