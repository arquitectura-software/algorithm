package mx.itesm.finalarquitectura.algorithm.endpoint;

import mx.itesm.finalarquitectura.algorithm.pojo.Message;
import mx.itesm.finalarquitectura.algorithm.pojo.MessageResult;
import mx.itesm.finalarquitectura.algorithm.service.DistanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Component
@Path("")
@Produces(MediaType.APPLICATION_JSON)
public class DistancesEndpoint {

    @Autowired
    private DistanceService distanceService;

    @POST
    @Path("/init")
    public Response init() {
        MessageResult result = distanceService.initDistances();
        Response response;
        if(result.getErrorCode() == null || result.getErrorCode() < 1) {
            response = Response.ok(result.getMessage()).build();
        } else if(result.getMessage() != null) {
            response = Response.status(result.getErrorCode()).entity(result.getMessage()).build();
        } else {
            response = Response.serverError().entity(new Message("Error de servidor.")).build();
        }
        return response;
    }
}
