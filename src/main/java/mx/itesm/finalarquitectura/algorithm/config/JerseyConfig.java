package mx.itesm.finalarquitectura.algorithm.config;

import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;
import mx.itesm.finalarquitectura.algorithm.endpoint.DistancesEndpoint;
import mx.itesm.finalarquitectura.algorithm.endpoint.ShortestRouteEndpoint;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.ServerProperties;
import org.springframework.stereotype.Component;

@Component
public class JerseyConfig extends ResourceConfig {
    public JerseyConfig() {
        register(JacksonJaxbJsonProvider.class);
        registerEndpoints();
        property(ServerProperties.BV_SEND_ERROR_IN_RESPONSE, true);
    }

    public void registerEndpoints() {
        register(ShortestRouteEndpoint.class);
        register(DistancesEndpoint.class);
    }
}
