FROM openjdk:8-jdk-alpine
VOLUME /tmp
COPY /target/algorithm-0.0.1-SNAPSHOT.jar app/app.jar
COPY ./distances_res.csv ./distances_res.csv
EXPOSE 8092
CMD ["java", "-Xmx200m", "-jar","/app/app.jar"]
